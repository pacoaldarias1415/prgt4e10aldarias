package prgt4e10aldarias;

/**
 * Fichero: While01.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class While01 {

  public static void main(String args[]) {
    int contador = 1;
    while (contador < 6) {
      System.out.print(contador + " ");
      contador++;
    }
  }
}

/* EJECUCION:
1 2 3 4 5
*/