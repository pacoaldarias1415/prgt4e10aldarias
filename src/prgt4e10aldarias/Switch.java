
package prgt4e10aldarias;

/**
 * Fichero: Switch.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Switch {

  public static void main(String args[]) {
    int x = 2;
    char y = 'b';
    String z = "BB";
    switch (x) {
      case 1:
        System.out.println("A");
        break;
      case 2:
        System.out.println("B");
        break;
      default:
        System.out.println("C");
        break;
    }
    switch (x / 2) {
      case 1:
        System.out.println("A");
        break;
      case 2:
        System.out.println("B");
        break;
      // case <2: System.out.println("B"); break; // Error
      // case 3,4: System.out.println("C"); break; // Error
      default:
        System.out.println("C");
        break;
    }
    switch (y) {
      case 'a':
        System.out.println("A");
        break;
      case 'b':
        System.out.println("B");
        break;
      default:
        System.out.println("C");
        break;
    }
    // switch (z)  // Error
  }
}

/* EJECUCION:
 B
 A
 B
 */
