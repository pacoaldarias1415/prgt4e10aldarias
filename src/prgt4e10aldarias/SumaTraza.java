
package prgt4e10aldarias;

/**
 * Fichero: SumaTraza.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */

public class SumaTraza {
  public static void main(String [] args) {
    int s=0,i=1;
    while (i<=3) {
      s=s+i;
      i++;
    }
    System.out.println("La suma es: "+s);
  }
}
/* Ejecucion:
La suma es: 6
*/