package prgt4e10aldarias;

/**
 * Fichero: Ifelse.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 10-nov-2013
 */
public class Ifelse {

  public static void main(String args[]) {
    int nota = 6;
    if (nota < 5) {
      System.out.println("Mal");
    }
    if (nota == 5) {
      System.out.println("Regular");
    }
    if (nota > 5) {
      System.out.println("Bien");
    }
  }
}
/* EJECUCION:
Bien
*/